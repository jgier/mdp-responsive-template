$(document).ready(function() {

//toggle the menu list on mobile
  $('.mblnav').on('touchstart click' , function(event){ 
  	$( ".toggle" ).slideToggle();
    return false;
  });

  //make sure the menu list displays on wider screens
  var windowsize = $(window).width();

  $(window).resize(function() {
    	windowsize = $(window).width();
    	if (windowsize > 769) {
    		$('.toggle').attr("style", "");
    	} 
  });

if (Modernizr.touch) {   

    $('.entry-content').find('img').unwrap();
   //alert('Touch Screen');
    //$('.post h1 a, .post a img').on('touchend click', function() {
      //    return false; 
    //});

}

//prevents standalone mode(iOS) links from opening iOS safari, unless target="_blank"
	$(document).on('touchend click', 'a', function(e) {

	    if ($(this).attr('target') !== '_blank') {
	        e.preventDefault();
	        window.location = $(this).attr('href');
	    }

	});

  // Begin Packery
  var $container = $('.postwrap');
  //Initialize Packery after all images have loaded, fixes overlap on load. 
  //Also have to include imagesloaded.min.js
  $('.postwrap').imagesLoaded( function() {
    // images have loaded
    $container.packery({
    itemSelector: '.post',
    });

    });// End Packery
	
});